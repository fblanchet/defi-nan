import cv2
import numpy
import numpy.random
import scipy.signal
import sys
import time


class grid_metadata:
    def __init__(self, select, shape):
        self.order = int(numpy.log2(numpy.sum(select)) + 1)
        self.shape = shape
        self.dtype = select.dtype
        self.state_max = (8 * self.dtype.itemsize) // self.order - 1
        print("Maximum number of state {}.".format(self.state_max))

    def state_id(self, num):
        return 1 << (num * self.order)

    def state_num(self, id):
        return numpy.log2(id).astype(numpy.uint8) // self.order


class conditional_from:
    def __init__(self, from_state, metadata, *conds):
        self.from_state = metadata.state_id(from_state)
        self.conditions = [
            (
                (metadata.state_max - cond_state) * metadata.order,
                cond_count << (metadata.state_max * metadata.order),
            )
            for cond_state, cond_count in conds
        ]
        self.result = numpy.empty(metadata.shape, dtype=bool)

    def shift_set(self):
        return set(shift for shift, _ in self.conditions)

    def __call__(self, grid, counts):
        numpy.equal(grid, self.from_state, out=self.result)
        for shift, cond_count in self.conditions:
            numpy.logical_and(self.result, counts[shift] >= cond_count, out=self.result)


class cache_convolver:
    def __init__(self, shape, kernel_shape, dtype=None):
        self.data_buffer = numpy.zeros(
            [s + s_kernel - 1 for s, s_kernel in zip(shape, kernel_shape)], dtype=dtype
        )
        self.result_buffer = numpy.empty(shape, dtype=dtype)

        self.view_data = self.data_buffer[
            kernel_shape[0] // 2 : -kernel_shape[0] // 2 + 1,
            kernel_shape[1] // 2 : -kernel_shape[1] // 2 + 1,
        ]
        self.strides_data = numpy.lib.stride_tricks.sliding_window_view(
            self.data_buffer, kernel_shape
        )

    def __call__(self, kernel):
        numpy.einsum(
            "ijmn,mn->ij",
            self.strides_data,
            kernel,
            out=self.result_buffer,
            casting="unsafe",
        )


class forwarder:
    def __init__(self, convolver, metadata, *transitions):
        self.convolver = convolver
        self.metadata = metadata
        self.transitions = transitions

        self.counts = dict()
        for candidate_cells, _, _ in self.transitions:
            for shift in candidate_cells.shift_set():
                if shift not in self.counts:
                    self.counts[shift] = numpy.empty(
                        metadata.shape, dtype=metadata.dtype
                    )

        self.generator = numpy.random.Generator(numpy.random.SFC64())
        self.rand_high = numpy.iinfo(numpy.uint32).max

    def __call__(self, select):
        self.convolver(select)
        rand = self.generator.integers(
            self.rand_high, size=self.metadata.shape, dtype=numpy.uint32, endpoint=True
        )

        for shift in self.counts:
            numpy.left_shift(
                self.convolver.result_buffer, shift, out=self.counts[shift]
            )

        for conditional, _, _ in self.transitions:
            conditional(self.convolver.view_data, self.counts)
        for conditional, next_state, cond_proba in self.transitions:
            mask_proba = rand[conditional.result] < cond_proba

            self.convolver.view_data[conditional.result] = numpy.where(
                mask_proba, next_state, self.convolver.view_data[conditional.result]
            )
            rand[conditional.result] = numpy.where(
                mask_proba, len(self.transitions), rand[conditional.result] - cond_proba
            )


def int_proba(proba):
    return numpy.uint32(proba * numpy.iinfo(numpy.uint32).max)


if __name__ == "__main__":
    sizex, sizey, fps, initial, max_cycles = (int(arg) for arg in sys.argv[1:])

    dtype = numpy.dtype(numpy.uint32)
    select = numpy.asarray(
        [
            [1, 1, 1],
            [1, 0, 1],
            [1, 1, 1],
        ],
        dtype=dtype,
    )

    metadata = grid_metadata(select, (sizex, sizey))
    colors = {
        metadata.state_id(0): (0x7F, 0xFF, 0x00),  # Jeune foret
        metadata.state_id(1): (0x22, 0x8B, 0x22),  # Ancienne foret
        metadata.state_id(2): (0xBD, 0xB7, 0x6B),  # Debut de combustion
        metadata.state_id(3): (0xB2, 0x22, 0x22),  # En combustion
        metadata.state_id(4): (0xFF, 0x8C, 0x0),  # Fin de combustion
        metadata.state_id(5): (0x0, 0x0, 0x0),  # Cendres
    }

    convolver = cache_convolver(metadata.shape, select.shape, dtype=metadata.dtype)
    convolver.view_data[:] = metadata.state_id(initial) * numpy.ones(
        metadata.shape, dtype=metadata.dtype
    )
    forward = forwarder(
        convolver,
        metadata,
        (conditional_from(0, metadata), metadata.state_id(1), int_proba(0.005)),
        #                 Jeune foret   -> Ancienne foret     Probalite 0.5%
        (conditional_from(0, metadata, (2, 1)), metadata.state_id(2), int_proba(0.01)),
        #                 Jeune foret           -> Debut de comb.     Probalite 1%
        #                              Si nb Debut de comb. >= 1
        (conditional_from(0, metadata, (3, 1)), metadata.state_id(2), int_proba(0.02)),
        #                 Jeune foret           -> Debut de comb.     Probalite 2%
        #                              Si nb En comb. >= 1
        (conditional_from(0, metadata, (4, 1)), metadata.state_id(2), int_proba(0.01)),
        #                 Jeune foret           -> Debut de comb.     Probalite 1%
        #                              Si nb Fin de comb. >= 1
        (conditional_from(1, metadata, (2, 1)), metadata.state_id(2), int_proba(0.1)),
        #                 Ancienne foret        -> Debut de comb.     Probalite 10%
        #                              Si nb Debut de comb. >= 1
        (conditional_from(1, metadata, (3, 1)), metadata.state_id(2), int_proba(0.2)),
        #                 Ancienne foret        -> Debut de comb.     Probalite 20%
        #                              Si nb En comb. >= 1
        (conditional_from(1, metadata, (4, 1)), metadata.state_id(2), int_proba(0.1)),
        #                 Ancienne foret        -> Debut de comb.     Probalite 10%
        #                              Si nb Fin de comb. >= 1
        (
            conditional_from(1, metadata, (1, 5)),
            #                Ancienne foret Si nb Jeune foret  >= 5
            metadata.state_id(2),
            # -> Debut de comb.
            int_proba(0.00005),
            # Probalite 0.005%
        ),
        (conditional_from(2, metadata), metadata.state_id(3), int_proba(0.1)),
        #                 Debut de comb.-> En comb.           Probalite 10%
        (conditional_from(3, metadata), metadata.state_id(4), int_proba(0.1)),
        #                 En comb.      -> Fin de comb.       Probalite 10%
        (conditional_from(4, metadata), metadata.state_id(5), int_proba(0.1)),
        #                 Fin de comb.  -> Cendres            Probalite 10%
        (conditional_from(5, metadata), metadata.state_id(0), int_proba(0.001)),
        #                 Cendres       -> Jeune foret        Probalite 0.1%
    )

    # OpenCV objects
    out = cv2.VideoWriter(
        "output.avi",
        cv2.VideoWriter_fourcc(*"mp4v"),
        fps,
        (metadata.shape[1], metadata.shape[0]),
    )
    image = numpy.empty(metadata.shape + (3,), dtype=numpy.uint8)

    try:
        start = time.process_time()
        for cycles in range(max_cycles):
            if cycles % fps == 0 and cycles > 0:
                print(
                    "{:.4g}% -- {:.4g} Cycle/s -- ETA {:.4g}s".format(
                        100 * cycles / max_cycles,
                        cycles / ((time.process_time() - start)),
                        (time.process_time() - start) * (max_cycles / cycles - 1),
                    )
                )
            forward(select)

            # Create OpenCV frames -- Not efficient
            for state_id, (r, g, b) in colors.items():
                state_mask = numpy.where(convolver.view_data == state_id)
                image[:, :, 0][state_mask] = b
                image[:, :, 1][state_mask] = g
                image[:, :, 2][state_mask] = r
            out.write(image)

    finally:
        out.release()
